﻿using Examenrdpenalbam.Controllers;
using Examenrdpenalbam.Models;
using System.Windows;
namespace Examenrdpenalbam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainController mc;

        public MainWindow()
        {
            InitializeComponent();
            setController();
        }

        public void setController()
        {
            mc = new MainController(this);
            btnAddItem.Click += new RoutedEventHandler(mc.ButtonEventHandler);

        }

        public void AddItemtoList()
        {
            LstBxList.Items.Add(txtBxItem.Text);
        }

        /*
        public Lista GetData()
        {
            Lista lista = new Lista
            {
                Name = txtBxListName.Text,
                //ListItems = LstBxList.L
                /*
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                BirthDate = (DateTime)BirthDatePicker.SelectedDate,
                //person.State = ((ComboBoxItem)StateCombobox.SelectedValue).Content.ToString();
                State = StateCombobox.SelectedValue.ToString()
            };
            return lista;
        }

        public void SetData(Person data)
        {
            FirstNameTextBox.DataContext = data;
            LastNameTextBox.DataContext = data;
            BirthDatePicker.DataContext = data;
            StateCombobox.DataContext = data;
        }
*/
        
    }
}
