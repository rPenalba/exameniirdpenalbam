﻿using Examenrdpenalbam.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examenrdpenalbam.Models 
{
    [Serializable]

    class Lista
    {
        public String Name { get; set; }
        public List<string> ListItems { get; set; }

        public Lista FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Lista>(filepath);
        }
        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }

    }
}
